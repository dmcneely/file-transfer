package filetransfer.com.filetransfer;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpATTRS;
import com.jcraft.jsch.SftpException;

import java.io.File;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, FileSystemViewAdapter.ItemClickListener {

    final int READ_EXTERNAL_STORAGE_PERMISSION_CODE = 99;
    FileSystemViewAdapter adapter;
    String path;
    String[] data;
    RecyclerView recyclerView;
    TextView emptyView, pathText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //check for permission

        if (!HasPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE))
        {
            //ask for permission
            ActivityCompat.requestPermissions(this,(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}), READ_EXTERNAL_STORAGE_PERMISSION_CODE);
        }

        path = Environment.getExternalStorageDirectory().toString();

        Log.d("Files", "Path: " + path);
        File directory = new File(path);
        File[] files = directory.listFiles();
        //Log.d("Files", "Size: "+ files.length);
        for (int i = 0; i < files.length; i++)
        {

            //printDirectory(files[i]);

          /*  if (files[i].isDirectory()){

                Log.d("Files", "Directory:" + files[i].getName());
                File[] subdirectory = files[i].listFiles();
                //Log.d("Files", "subdirectory Size: "+  subdirectory.length);
                for (int j = 0; j < subdirectory.length; j++) {

                    Log.d("Files", "Subfile  FileName:" + subdirectory[j].getName());
                }
            }
            else {
                Log.d("Files", "File:" + files[i].getName());
            }*/
        }

        // data to populate the RecyclerView with
        data = new String[files.length];
        for (int i = 0; i < files.length; i++) {

            data[i] = files[i].getName();
            Log.d("Files", "File:" + files[i].getName());
        }

        emptyView = findViewById(R.id.empty_view);
        pathText = findViewById(R.id.path);
        pathText.setText(path);

        // set up the RecyclerView
        //RecyclerView recyclerView = findViewById(R.id.filestructure);
        //ColumnQty columnQty = new ColumnQty(this, R.layout.recycleview_item);
        //int numberOfColumns = 4;
        //Log.d("columnQty", "columnQty.calculateNoOfColumns()" + columnQty.calculateNoOfColumns());

        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.addItemDecoration(new MarginDecoration(this));

        //recyclerView.setLayoutManager(new GridLayoutManager(this, numberOfColumns));
        //ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(this, R.dimen.item_offset);
        //recyclerView.addItemDecoration(itemDecoration);

        adapter = new FileSystemViewAdapter(this, data);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);

        new AsyncTask<Integer, Void, Void>(){
            @Override
            protected Void doInBackground(Integer... params) {
                try {
                    ssh_client();

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute(1);

    }

    @Override
    public void onItemClick(View view, int position) {
        Log.i("TAG", "You clicked number " + adapter.getItem(position) + ", which is at cell position " + position);

        path += '/' + adapter.getItem(position);
        pathText.setText(path);

        Log.i("TAG", "new path = " + path);

        //data(position)

        File directory = new File(path);
        //printDirectory(directory);

        Toast.makeText(this, adapter.getItem(position), Toast.LENGTH_SHORT).show();

        if (directory.listFiles() != null) {
            data = new String[directory.listFiles().length];
        }
        else {
            data = new String[0];
        }

        Log.d("Files", "number of files " + data.length);
        for (int i = 0; i < data.length; i++) {

            data[i] = directory.listFiles()[i].getName();
            Log.d("Files", "File:" + directory.listFiles()[i].getName());
        }

        if (adapter !=null){
            adapter.setItems(data);
            adapter.notifyDataSetChanged();

            if (data.length == 0) {
                recyclerView.setVisibility(View.GONE);
                emptyView.setVisibility(View.VISIBLE);
            }
            else {
                recyclerView.setVisibility(View.VISIBLE);
                emptyView.setVisibility(View.GONE);
            }
        }
    }

    private void ssh_client (){
        try {
            JSch ssh = new JSch();
            Session session = ssh.getSession("black", "blackapi.com", 4444);
            String privateKey = Environment.getExternalStorageDirectory().toString() + "/Download/ec2_darren.pem";
            ssh.addIdentity(privateKey);

            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);

            session.connect();

            Channel channel = session.openChannel("sftp");
            channel.connect();

            ChannelSftp sftp = (ChannelSftp) channel;

            // Now that we have a channel, go to a directory first if we want .. you can give to the ls the path
            // sftp.cd("/var/www/mydirectory");

            @SuppressWarnings("unchecked")

            // Get the content of the actual path using ls instruction or use the previous string of the cd instruction
            java.util.Vector<ChannelSftp.LsEntry> fileList = sftp.ls("/home/black");


            // show the info of every folder/file in the console
     /*       for(int j = 0; j< fileList.size() ;j++){
                ChannelSftp.LsEntry entry = fileList.get(j);
                SftpATTRS attr = entry.getAttrs();

                Log.i("TAG", entry.getFilename());
                //System.out.println(directory + "/" + entry.getFilename()); // Remote filepath
                Log.i("TAG","isDir " +  attr.isDir()); // Is folder
                Log.i("TAG","isLink " +  attr.isLink()); // is link
                Log.i("TAG","size " + attr.getSize()); // get size in bytes of the file
                Log.i("TAG","permissions "  + attr.getPermissions()); // permissions
                Log.i("TAG","permissions_string " + attr.getPermissionsString());
                Log.i("TAG","longname " + entry.toString());
            }*/

            if (fileList.size() != 0) {
                data = new String[fileList.size()];
            }
            else {
                data = new String[0];
            }

            Log.d("Files", "number of files " + data.length);
            for (int i = 0; i < data.length; i++) {

                data[i] = fileList.get(i).getFilename();
                Log.d("Files", "File:" +  fileList.get(i).getFilename());
            }

            channel.disconnect();
            session.disconnect();
        } catch (JSchException e) {
            Log.e("TAG",e.getMessage());
            e.printStackTrace();
        } catch (SftpException e) {
            Log.e("TAG",e.getMessage());
            e.printStackTrace();
        }
    }

    private void printDirectory (File directory){
        if (directory.isDirectory()){

            Log.d("Files", "Directory:" + directory.getName());
            File[] subdirectory = directory.listFiles();
            //Log.d("Files", "subdirectory Size: "+  subdirectory.length);
            for (int j = 0; j < subdirectory.length; j++) {

                Log.d("Files", "Subfile  FileName:" + subdirectory[j].getName());

                //printDirectory(subdirectory[j]);
            }
        }
        else {
            Log.d("Files", "File:" + directory.getName());
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            //super.onBackPressed();

            File directory = new File(path).getParentFile();
            Log.i("TAG", "Back press path = " + directory);
            path = directory.getPath();
            pathText.setText(path);

            if (directory.listFiles() != null) {
                data = new String[directory.listFiles().length];
            }
            else {
                data = new String[0];
            }

            Log.d("Files", "number of files " + data.length);
            for (int i = 0; i < data.length; i++) {

                data[i] = directory.listFiles()[i].getName();
                Log.d("Files", "File:" + directory.listFiles()[i].getName());
            }

            if (adapter !=null){
                adapter.setItems(data);
                adapter.notifyDataSetChanged();

                if (data.length == 0) {
                    recyclerView.setVisibility(View.GONE);
                    emptyView.setVisibility(View.VISIBLE);
                }
                else {
                    recyclerView.setVisibility(View.VISIBLE);
                    emptyView.setVisibility(View.GONE);
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private boolean HasPermission(Context context, String permission) {
        return (ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED);
    }
}
